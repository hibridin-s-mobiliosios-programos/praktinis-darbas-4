# Praktinis Darbas 4

<div align="center">
  <b>Studentas</b>: Emilis Žvirblys<br><b>Grupė</b>: PI20C<br><b>Dėstytojas</b>: J. Zailskas
</div>

## Requirements

Create a **Expo React Native** project that utilizes a barcode scanner library to scan and take product data from a firebase realtime database.

## Sources used

1. [BarcodeScanner - Expo Documentation](https://docs.expo.dev/versions/latest/sdk/bar-code-scanner/)
2. [Using Firebase - Expo Documentation](https://docs.expo.dev/guides/using-firebase/)
3. [Firebase Realtime Database Structure](https://firebase.google.com/docs/firestore/manage-data/structure-data)
