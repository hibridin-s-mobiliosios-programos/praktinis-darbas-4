// Imports
import { initializeApp } from "firebase/app";
import { getDatabase } from "firebase/database";
import { getAuth } from 'firebase/auth';

// Your web app's Firebase configuration
const firebaseConfig = {
    appId: "1:818645420670:web:1d11c9aab3ea8543e3e62f",
    apiKey: "AIzaSyCNbkC4FlrswGs81mg6fklOFKOotsETHSY",
    projectId: "barcodescanner-78e27",
    authDomain: "barcodescanner-78e27.firebaseapp.com",
    databaseURL: "https://barcodescanner-78e27-default-rtdb.europe-west1.firebasedatabase.app",
    storageBucket: "barcodescanner-78e27.appspot.com",
    messagingSenderId: "818645420670"
};

const app = initializeApp(firebaseConfig); // Firebase
const database = getDatabase(app); // Realtime database
const auth = getAuth(app); // Authentication

export { auth, database };