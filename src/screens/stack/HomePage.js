﻿import React, {
    useState,
    useEffect
} from 'react';

import {
    View,
    Text,
    Alert,
    Button,
    FlatList,
    Dimensions,
    StyleSheet,
    SafeAreaView
} from 'react-native';

import uuid from 'react-native-uuid';

import {
    BarCodeScanner
} from 'expo-barcode-scanner';

import {
    getAuth,
    onAuthStateChanged
} from 'firebase/auth';

import {
    ref,
    set,
    get,
    child
} from "firebase/database";

import {
    database
} from '../../FirebaseConfig';

function HomePage({ navigation: { navigate } }) {
    const [hasPermission, setHasPermission] = useState(null);
    const [scanned, setScanned] = useState(false);
    const [itemCart, setItemCart] = useState([]);

    var currentUser;
    const auth = getAuth();

    useEffect(() => {
        const getBarCodeScannerPermissions = async () => {
            const { status } = await BarCodeScanner.requestPermissionsAsync();
            setHasPermission(status === 'granted');
        };

        getBarCodeScannerPermissions();
    }, []);

    onAuthStateChanged(auth, (user) => {
        if (user) {
            currentUser = user;
        } else {
            navigation.navigate('Login');
        }
    });

    const logOff = () => {
        auth.signOut().then(function () {
            navigation.navigate('Login');
        }).catch(function (error) {
            console.error(error);
        });
    };

    const handleBarCodeScanned = ({ type, data }) => {
        setScanned(true);
        console.log('Scanned item: ' + data);
        getProduct(data);
    };

    const getProduct = (data) => {
        const reference = ref(database);
        get(child(reference, 'products/' + data))
            .then((result) => {
                if (result.exists()) {
                    var product = {
                        code: data,
                        name: result.val().name,
                        price: result.val().price,
                        quantity: 1
                    };
                    addProduct(product);
                } else console.log('No data available!');
            }).catch((error) => {
                console.error(error);
            });
    };

    const addProduct = (data) => {
        if (itemCart.some(item => item.code === data.code)) {
            setItemCart(itemCart => itemCart.map(item => item.code === data.code
                ? { ...item, quantity: item.quantity + 1 } : item
            ));
        } else
            setItemCart(itemCart => [...itemCart, data]);
    }

    const removeProduct = (code) => {
        const updatedCart = itemCart.filter(item => item.code !== code);
        setItemCart(updatedCart);
    }

    function writeProductData(user) {
        if (itemCart.length <= 0) {
            console.log('Cart is empty!');
            return;
        }

        set(ref(database, 'orders/' + uuid.v4()), {
            orderedBy: user.email,
            products: itemCart
        });

        Alert.alert(
            "Success",
            "Order created!",
            [
                { text: "OK" }
            ]
        );

        setItemCart([]);
    }

    return (
        <SafeAreaView style={{ flex: 1 }}>
            <View style={styles.header}>
                <View style={styles.headerRow}>
                    <Text style={styles.headerText}>Home page</Text>
                </View>
                <View style={styles.headerRow}>
                    <Button title='LIST' onPress={() => navigate('Cart')} />
                </View>
            </View>
            <View style={styles.scanView}>
                    <View style={styles.barcodeBox}>
                        <BarCodeScanner onBarCodeScanned={scanned ? undefined : handleBarCodeScanned} style={styles.barcodeScanner} />
                </View>
                <View>
                    {scanned && <Button title="Tap to scan again..." onPress={() => setScanned(false)} />}
                </View>
            </View>

            <View style={styles.listHeader}>
                <View style={styles.headerRow}>
                    <Text style={styles.title}>Cart list:</Text>
                </View>
                <View style={styles.headerRow}>
                    <Button title='BUY' onPress={() => writeProductData(currentUser)} />
                </View>
            </View>

            <View>
                <FlatList
                    style={styles.list}
                    data={itemCart}
                    renderItem={(item) => {
                        return (
                            <View style={styles.row}>
                                <Text style={styles.rowText}>Name: {item.item.name}</Text>
                                <Text style={styles.rowText}>Quantity: {item.item.quantity}</Text>
                                <Button title='Delete product' style={{ alignSelf: 'center' }} onPress={() => removeProduct(item.item.code)} />
                            </View>
                        )
                    }}
                    keyExtractor={ item => item.code} />
            </View>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    header: {
        height: 65,
        padding: 5,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: "slategray"
    },

    headerText: {
        fontSize: 22,
        fontWeight: 'bold',
        textAlign: 'center',
    },

    scanView: {
        height: 400,
        padding: 20,
        backgroundColor: 'darkslategrey'
    },

    barcodeBox: {
        marginBottom: 10,
        alignItems: 'center',
        justifyContent: 'center',
        height: 300,
        overflow: 'hidden',
        borderRadius: 30
    },

    barcodeScanner: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height
    },

    listHeader: {
        padding: 5,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'lightsalmon'
    },

    headerRow: {
        flex: 1,
    },

    list: {
        padding: 5
    },

    title: {
        fontSize: 20,
        fontWeight: 'bold',
        textAlign: 'center',
    },

    row: {
        padding: 10,
        marginVertical: 10,
        marginHorizontal: 10,
        backgroundColor: 'wheat'
    },

    rowText: {
        fontSize: 18,
        marginBottom: 5
    }
});

export default HomePage;