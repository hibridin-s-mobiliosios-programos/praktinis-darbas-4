import React, {
    useState,
    useEffect
} from 'react';

import {
    View,
    Text,
    Alert,
    Button,
    FlatList,
    StyleSheet,
    SafeAreaView
} from 'react-native';

import {
    ref,
    remove,
    onValue
} from 'firebase/database';

import {
    database
} from '../../FirebaseConfig';

function CartPage({ navigation: { navigate } }) {
    const [orders, setOrders] = useState([]);
    const reference = ref(database, '/orders');

    useEffect(() => {
        onValue(reference, (snapshot) => {
            snapshot.forEach((childSnapshot) => {
                const childKey = childSnapshot.key;
                const childData = childSnapshot.val();
                console.log('Key:' + childKey);
                console.log('Data:' + JSON.stringify(childData));
                const order = {
                    id: childKey,
                    orderedBy: childData.orderedBy,
                    products: childData.products
                };
                setOrders(emptyArray => [...emptyArray, order]);
            })
        }, {
            onlyOnce: true
        });
    }, []);

    const deleteOrder = (id) => {
        const orderRef = ref(database, 'orders/' + id);
        remove(orderRef).then(() => {
            Alert.alert(
                "Success",
                "Order removed!",
                [
                    { text: "OK" }
                ]
            );
        });
    };

    return (
        <SafeAreaView style={{ flex: 1 }}>
            <View style={styles.header}>
                <Text style={styles.headerText}>Cart Page</Text>
            </View>

            <FlatList
                style={styles.list}
                data={orders}
                renderItem={(item) => {
                    return (
                        <View style={styles.row}>
                            <Text style={styles.rowTitle}>Ordered by: {item.item.orderedBy}</Text>
                            <Text>Products:</Text>
                            <FlatList
                                style={styles.list}
                                data={item.item.products}
                                renderItem={(item) => {
                                    return (
                                        <View style={styles.row}>
                                            <Text style={styles.rowTitle}>Code: {item.item.code}</Text>
                                            <Text style={styles.rowTitle}>Name: {item.item.name}</Text>
                                            <Text style={styles.rowTitle}>Price: {item.item.price}</Text>
                                            <Text style={styles.rowTitle}>Quantity: {item.item.quantity}</Text>
                                        </View>
                                    )
                                }}
                                keyExtractor={item => item.code}
                                scrollEnabled={true}
                            />
                            <Button title='DELETE' onPress={() => deleteOrder(item.item.id)} />
                        </View>
                    )
                }}
                keyExtractor={item => item.id}
                scrollEnabled={true}
            />
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    header: {
        height: 65,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'slategray'
    },

    headerText: {
        fontSize: 22,
        fontWeight: 'bold'
    },

    list: {
        padding: 15
    },

    row: {
        padding: 5,
        borderWidth: 1,
        borderColor: '#ddd',
        borderRadius: 15,
        marginBottom: 5,
        backgroundColor: '#ffffff'
    },

    rowTitle: {
        color: 'black',
        fontSize: 18,
        fontWeight: 'bold',
        marginBottom: 15
    }
});

export default CartPage;