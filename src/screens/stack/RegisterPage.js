import React, {
    useState
} from 'react';

import {
    View,
    Text,
    StyleSheet,
    SafeAreaView,
    TouchableOpacity
} from 'react-native';

import {
    getAuth,
    createUserWithEmailAndPassword
} from 'firebase/auth';

import CustomTextInput from '../../components/CustomTextInput';
import { NavigationHelpersContext } from '@react-navigation/native';

function RegisterPage({ navigation: { navigate } }) {
    const auth = getAuth();
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [passwordError, setPasswordError] = useState(false);

    const passwordErrorChange = (value) => {
        if (value.length < 4) {
            setPassword(true);
            setPasswordError(true);
        } else {
            setPassword(value);
            setPasswordError(false);
        }
    }

    const registerUser = () => {
        const user = {
            email: email,
            password: password
        }

        console.log('Registration data:\r\n\tEmail: ' + user.email + "\r\n\tPassword: " + user.password);

        createUserWithEmailAndPassword(auth, email, password)
            .then((userCredential) => {
                alert("Registration successful!");
                NavigationHelpersContext('LoginPage');
            }).catch(error => {
                console.log("=== === === === ===");
                console.log("Error code: " + error.code);
                console.log("Message: " + error.message);
                console.log("=== === === === ===");
            });
    }

    return (
        <SafeAreaView style={[styles.SafeAreaView]}>
            <View style={[styles.header]}>
                <Text style={[styles.text]}>Register Page...</Text>
            </View>

            <View style={[styles.body]}>
                <CustomTextInput
                    title="Email"
                    placeholder="Enter your email..."
                    onChangeText={(email) => setEmail(email)}
                />

                <CustomTextInput
                    title="Password"
                    placeholder="Enter your password..."
                    onChangeText={(password) => passwordErrorChange(password)}
                />

                {passwordError ? (
                    <View>
                        <Text>Password must be longer than 3 characters!</Text>
                    </View>
                ) : null}

                <TouchableOpacity onPress={() => registerUser()}>
                    <Text>Register</Text>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => navigate('Login')}>
                    <Text>Already have an account? Press here!</Text>
                </TouchableOpacity>
            </View>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    safeAreaView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },

    header: {
        height: 65,
        backgroundColor: 'slategray',
        justifyContent: 'center',
        alignItems: 'center'
    },

    body: {
        padding: 20
    },

    text: {
        fontSize: 22,
        fontWeight: 'bold'
    },

    hint: {
        fontSize: 12,
        fontStyle: 'italic'
    }
});

export default RegisterPage;
