﻿import React, {
    useState,
    Component,
    useEffect
} from 'react';

import {
    Text,
    View,
    StyleSheet,
    SafeAreaView,
    TouchableOpacity
} from 'react-native';

import {
    BarCodeScanner
} from 'expo-barcode-scanner';

import {
    getAuth,
    signInWithEmailAndPassword
} from 'firebase/auth';

import CustomTextInput from '../../components/CustomTextInput';

class LoginPage extends Component {
    state = {
        email: "",
        password: "",
        loading: true,
        CameraPermissionGranted: null
    }

    async componentDidMount() {
        const { status } = await BarCodeScanner.requestPermissionsAsync();
        this.setState({ CameraPermissionGranted: status === 'granted' ? true : false, loading: false })
    }

    render() {
        const auth = getAuth();
        const { CameraPermissionGranted } = this.state;

        if (CameraPermissionGranted === null) {
            return (
                <SafeAreaView>
                    <Text>Camera permission is null!</Text>
                </SafeAreaView>
            );
        }

        if (CameraPermissionGranted === false) {
            return (
                <SafeAreaView style={styles.container}>
                    <Text>App does not have access to camera permission!\r\nPlease enable them through the app settings.</Text>
                </SafeAreaView>
            );
        }

        const loginUser = () => {
            if (this.state.email === "" || this.state.email === null) return;
            if (this.state.password === "" || this.state.password === null) return;

            signInWithEmailAndPassword(auth, this.state.email, this.state.password)
                .then((userCredential) => {
                    const user = userCredential.user;
                    this.props.navigation.navigate('Home');
                }).catch(error => {
                    console.log("=== === === === ===");
                    console.log("Error code: " + error.code);
                    console.log("Message: " + error.message);
                    console.log("=== === === === ===");
                });
        }

        return (
            <SafeAreaView style={[styles.safeAreaView]}>
                <View style={[styles.header]}>
                    <Text style={[styles.text]}>Login Page...</Text>
                </View>

                <View style={[styles.body]}>
                    <CustomTextInput
                        title="Email"
                        placeholder="Enter your email..."
                        onChangeText={(value) => this.setState({ email: value })}
                    />

                    <CustomTextInput
                        title="Password"
                        placeholder="Enter your password..."
                        onChangeText={(value) => this.setState({ password: value })}
                    />

                    <TouchableOpacity onPress={() => loginUser()}>
                        <Text>Login</Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Register')}>
                        <Text>Don't have an account? Press here!</Text>
                    </TouchableOpacity>
                </View>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    safeAreaView: {
        flex: 1,
    },

    header: {
        height: 65,
        backgroundColor: 'slategray',
        justifyContent: 'center',
        alignItems: 'center'
    },

    body: {
        padding: 20
    },

    text: {
        fontSize: 22,
        fontWeight: 'bold'
    },

    hint: {
        fontSize: 12,
        fontStyle: 'italic'
    }
});

export default LoginPage;