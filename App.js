import React from 'react';

import {
    NavigationContainer
} from '@react-navigation/native';

import {
    createNativeStackNavigator
} from '@react-navigation/native-stack';

import LoginPage from './src/screens/stack/LoginPage';
import RegisterPage from './src/screens/stack/RegisterPage';
import HomePage from './src/screens/stack/HomePage';
import CartPage from './src/screens/stack/CartPage';

const Stack = createNativeStackNavigator();

function App() {
    return (
        <NavigationContainer>
            <Stack.Navigator initialRouteName={LoginPage}>
                <Stack.Screen
                    name="Login"
                    options={{ title: 'Login page', headerShown: false }}
                    component={LoginPage}
                />

                <Stack.Screen
                    name="Register"
                    options={{ title: 'Register page', headerShown: false }}
                    component={RegisterPage}
                />

                <Stack.Screen
                    name="Home"
                    options={{ title: 'Home page', headerShown: false }}
                    component={HomePage}
                />

                <Stack.Screen
                    name="Cart"
                    options={{ title: 'Cart page', headerShown: false }}
                    component={CartPage}
                />
            </Stack.Navigator>
        </NavigationContainer>
    );
};

export default App;